package com.stepstone;

import com.stepstone.config.StaticString;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) // for restTemplate
@ActiveProfiles("test")
public class AuthorControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void find_author_OK() throws JSONException {

        String expected = "[{\"tilesCount\":0,\"authorID\":\"2121909\"},{\"tilesCount\":0,\"authorID\":\"172346\"},{\"tilesCount\":0,\"authorID\":\"51853\"},{\"tilesCount\":0,\"authorID\":\"55404\"},{\"tilesCount\":0,\"authorID\":\"2075096\"},{\"tilesCount\":0,\"authorID\":\"112479\"},{\"tilesCount\":0,\"authorID\":\"174231\"},{\"tilesCount\":0,\"authorID\":\"2093519\"},{\"tilesCount\":0,\"authorID\":\"226939\"},{\"tilesCount\":5,\"authorID\":\"141447\"}]";

        ResponseEntity<String> response = restTemplate.getForEntity("/authorsSearch?lastName=Brown&firstName=", String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON_UTF8, response.getHeaders().getContentType());
        System.out.println(response.getBody());
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    @Test
    public void not_find_author() {
        System.err.println(StaticString.err_test_info);
        ResponseEntity<String> response = restTemplate.getForEntity("/authorsSearch?lastName=karimi&firstName=", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON_UTF8, response.getHeaders().getContentType());
    }

    @Test
    public void not_find_author_with_out_parameter() {
        System.err.println(StaticString.err_test_info);
        ResponseEntity<String> response = restTemplate.getForEntity("/authorsSearch", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON_UTF8, response.getHeaders().getContentType());
    }
}
