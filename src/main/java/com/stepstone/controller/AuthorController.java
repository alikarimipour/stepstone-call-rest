package com.stepstone.controller;

import com.stepstone.service.AuthorService;
import com.stepstone.to.AuthorTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthorController {

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    // Find
    @GetMapping("/authorsSearch")
    ResponseEntity<List<AuthorTO>>  findAll(@RequestParam(defaultValue = "") String firstName, @RequestParam(defaultValue = "") String lastName) {
        List<AuthorTO> authorDetail = authorService.getAuthorDetail(firstName, lastName);
       return ResponseEntity.ok(authorDetail);
    }


}
