package com.stepstone.service;

import com.stepstone.client.Api;
import com.stepstone.error.AuthorNotFoundException;
import com.stepstone.to.Author;
import com.stepstone.to.AuthorTO;
import com.stepstone.to.Authors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorService {
    private final Api api;

    @Autowired
    public AuthorService(Api api) {
        this.api = api;
    }

    public List<AuthorTO> getAuthorDetail(String firstName, String lastName) {
        Authors authors = api.callRest(firstName, lastName);
        List<AuthorTO> authorTOS = new ArrayList<>();
        if (authors.getAuthor() == null || authors.getAuthor().size() == 0) {
            throw new AuthorNotFoundException(firstName,lastName);
        }
        for (Author author : authors.getAuthor()) {
            AuthorTO authorTO = new AuthorTO();
            if (author.getTitles()==null){
                authorTO.setTilesCount(0);
            }else {
                authorTO.setTilesCount(author.getTitles().getIsbn().size());
            }
            authorTO.setAuthorID(author.getAuthorid());
            authorTOS.add(authorTO);
        }
        return authorTOS;
    }
}
