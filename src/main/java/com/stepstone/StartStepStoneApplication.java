package com.stepstone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartStepStoneApplication {

    // start everything
    public static void main(String[] args) {
        SpringApplication.run(StartStepStoneApplication.class, args);
    }
}