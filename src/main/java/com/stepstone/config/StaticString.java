package com.stepstone.config;

public class StaticString {
    public static String restURL = "https://reststop.randomhouse.com/resources/authors?lastName=%s&firstName=%s";
    public static String app_json = "application/json";
    public static String failed = "Failed : HTTP error code : ";
    public static String server_output = "Output from Server .... \n";
    public static String author_not_found_err = "Author with name %s and lastName %s not found : ";
    public static String err_test_info = "this error made manually to test";


}
