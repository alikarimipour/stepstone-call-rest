package com.stepstone.error;

import com.stepstone.config.StaticString;

public class AuthorNotFoundException extends RuntimeException {

    public AuthorNotFoundException(String name, String family) {
        super(String.format(StaticString.author_not_found_err, name, family));
    }

}
