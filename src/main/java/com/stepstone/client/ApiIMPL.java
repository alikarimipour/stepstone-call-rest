package com.stepstone.client;

import com.google.gson.Gson;
import com.stepstone.config.StaticString;
import com.stepstone.error.AuthorNotFoundException;
import com.stepstone.to.Authors;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.springframework.stereotype.Component;

@Component
public class ApiIMPL implements Api {


    public Authors callRest(String firstName, String lastName) {
        Authors authors;

        Client client = Client.create();
        String s = String.format(StaticString.restURL, lastName, firstName);
        System.out.println(s);
        WebResource webResource = client
                .resource(s);

        ClientResponse response = webResource.accept(StaticString.app_json)
                .get(ClientResponse.class);

        if (response.getStatus() != 200) {
            if (response.getStatus() == 404) {
                throw new AuthorNotFoundException(firstName,lastName);
            }else {
                throw new RuntimeException(StaticString.failed
                        + response.getStatus());
            }

        }


        String output = response.getEntity(String.class);

        System.out.println(StaticString.server_output);
        System.out.println(output);
        Gson gson = new Gson();
        authors = gson.fromJson(output, Authors.class);
        return authors;
    }
}