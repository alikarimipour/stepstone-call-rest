package com.stepstone.client;

import com.stepstone.error.AuthorNotFoundException;
import com.stepstone.to.Authors;

public interface Api {
    Authors callRest(String firstName, String lastName) throws AuthorNotFoundException;

}
