
package com.stepstone.to;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Isbn {

    @SerializedName("@contributortype")
    @Expose
    private String contributortype;
    @SerializedName("$")
    @Expose
    private String $;

    public String getContributortype() {
        return contributortype;
    }

    public void setContributortype(String contributortype) {
        this.contributortype = contributortype;
    }

    public String get$() {
        return $;
    }

    public void set$(String $) {
        this.$ = $;
    }

}
