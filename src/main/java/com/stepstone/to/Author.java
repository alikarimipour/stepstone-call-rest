
package com.stepstone.to;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Author {

    @SerializedName("@uri")
    @Expose
    private String uri;
    @SerializedName("approved")
    @Expose
    private String approved;
    @SerializedName("authordisplay")
    @Expose
    private String authordisplay;
    @SerializedName("authorfirst")
    @Expose
    private String authorfirst;
    @SerializedName("authorfirstlc")
    @Expose
    private String authorfirstlc;
    @SerializedName("authorid")
    @Expose
    private String authorid;
    @SerializedName("authorlast")
    @Expose
    private String authorlast;
    @SerializedName("authorlastfirst")
    @Expose
    private String authorlastfirst;
    @SerializedName("authorlastlc")
    @Expose
    private String authorlastlc;
    @SerializedName("titles")
    @Expose
    private Titles titles;
    @SerializedName("lastinitial")
    @Expose
    private String lastinitial;
    @SerializedName("photocredit")
    @Expose
    private String photocredit;
    @SerializedName("photodate")
    @Expose
    private String photodate;
    @SerializedName("spotlight")
    @Expose
    private String spotlight;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public String getAuthordisplay() {
        return authordisplay;
    }

    public void setAuthordisplay(String authordisplay) {
        this.authordisplay = authordisplay;
    }

    public String getAuthorfirst() {
        return authorfirst;
    }

    public void setAuthorfirst(String authorfirst) {
        this.authorfirst = authorfirst;
    }

    public String getAuthorfirstlc() {
        return authorfirstlc;
    }

    public void setAuthorfirstlc(String authorfirstlc) {
        this.authorfirstlc = authorfirstlc;
    }

    public String getAuthorid() {
        return authorid;
    }

    public void setAuthorid(String authorid) {
        this.authorid = authorid;
    }

    public String getAuthorlast() {
        return authorlast;
    }

    public void setAuthorlast(String authorlast) {
        this.authorlast = authorlast;
    }

    public String getAuthorlastfirst() {
        return authorlastfirst;
    }

    public void setAuthorlastfirst(String authorlastfirst) {
        this.authorlastfirst = authorlastfirst;
    }

    public String getAuthorlastlc() {
        return authorlastlc;
    }

    public void setAuthorlastlc(String authorlastlc) {
        this.authorlastlc = authorlastlc;
    }

    public Titles getTitles() {
        return titles;
    }

    public void setTitles(Titles titles) {
        this.titles = titles;
    }

    public String getLastinitial() {
        return lastinitial;
    }

    public void setLastinitial(String lastinitial) {
        this.lastinitial = lastinitial;
    }

    public String getPhotocredit() {
        return photocredit;
    }

    public void setPhotocredit(String photocredit) {
        this.photocredit = photocredit;
    }

    public String getPhotodate() {
        return photodate;
    }

    public void setPhotodate(String photodate) {
        this.photodate = photodate;
    }

    public String getSpotlight() {
        return spotlight;
    }

    public void setSpotlight(String spotlight) {
        this.spotlight = spotlight;
    }

}
