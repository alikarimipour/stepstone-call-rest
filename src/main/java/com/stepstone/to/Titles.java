
package com.stepstone.to;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Titles {

    @SerializedName("isbn")
    @Expose
    private List<Isbn> isbn = null;

    public List<Isbn> getIsbn() {
        return isbn;
    }

    public void setIsbn(List<Isbn> isbn) {
        this.isbn = isbn;
    }

}
