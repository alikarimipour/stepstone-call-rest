package com.stepstone.to;

public class AuthorTO {
    private Integer tilesCount;
    private String authorID;

    public Integer getTilesCount() {
        return tilesCount;
    }

    public void setTilesCount(Integer tilesCount) {
        this.tilesCount = tilesCount;
    }

    public String getAuthorID() {
        return authorID;
    }

    public void setAuthorID(String authorID) {
        this.authorID = authorID;
    }
}
