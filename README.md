# Spring Step stone REST call project


## 1. How to start
```
$ git clone https://gitlab.com/alikarimipour/stepstone-call-rest.git
$ mvn spring-boot:run

curl -X GET "http://localhost:8585/authorsSearch?firstName=firsName&lastName=lastName" -H "accept: */*"
```
swagger ui(for test and documentation online):

http://localhost:8585/swagger-ui.html

swagger json(usable on Postman and similar app):

http://localhost:8585/v2/api-docs


and also you can see the result online with below link:

https://step-stone-call-web-service.herokuapp.com/swagger-ui.html
